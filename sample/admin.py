from flask_cfmm.core import Admin as AdminBase
from flask_cfmm.model_views import UserView, RoleView

from . import db
from .models import User, Role


class Admin(AdminBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_view(UserView(User, db.session, endpoint="user"))
        self.add_view(RoleView(Role, db.session, endpoint="role"))
