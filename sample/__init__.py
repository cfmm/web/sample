import typing as t

from flask import Flask
from flask_cfmm import CFMMFlask, create_database
from flask_cfmm.configure import configure_defaults, load_configuration
from flask_admin import AdminIndexView

cfmm = CFMMFlask()
db = create_database()


def create_app(
    config: str = None, mapping: t.Optional[t.Mapping[str, t.Any]] = None
) -> Flask:
    app = Flask(__name__)

    load_configuration(app, config)
    app.config.from_envvar("SAMPLE_CONFIG", silent=True)
    app.config.from_mapping(mapping)
    configure_defaults(app)

    from .models import User

    cfmm.init_app(app, db, user_model=User)

    from .admin import Admin

    # Flask-Admin initialization does not work correctly without using factory paradigm
    # i.e. do not pass app into initializer
    admin = Admin(
        app=app,
        base_template="cfmm/base.html",
        static_url_path=app.config.get("STATIC_URL_PATH") or "/admin/static",
        index_view=AdminIndexView(name="Home", endpoint="admin", url="/"),
    )

    return app
