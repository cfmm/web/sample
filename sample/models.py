from flask_cfmm import RoleMixin, UserMixin

from . import db


class Role(RoleMixin, db.Model):
    pass


class User(UserMixin, db.Model):
    role_model = Role
